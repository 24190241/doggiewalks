import React from "react";
import banner from "./images/photo-1531388185832-f111bb261e01.jpg";
import puppy from "./images/dog-young-dog-puppy-59965.jpeg";

function Home() {
    return (
        <main>
            <article className="banner">
                <img src={banner} alt="dog in a forrest" />
                <div><h3>Helping our animal friends one paw at a time</h3></div>
                <section class="offer">
                    <h2>50% off home visits for new customers!</h2>
                </section>
            </article>
            <article className="content home">
                <h1>Welcome to Doggie Walks! We are committed to providing 
                    professional and personal care to your beloved four-legged friends.</h1>
                <p>We are based in Ormskirk, we offer exciting and stimulating walks all 
                    over the beautiful Lancashire and Merseyside countryside. We guarantee 
                    your dog will come home tired and content.</p>
                <p>We recognise every dog's needs are different and how busy daily life 
                    can be. With services starting from only £10 we ensure you the best 
                    possible service to suit the needs of both you and your pooch.</p>
                <p>We are also fully insured for everybody's peace of mind, for more 
                    information on this please <span>contact us</span> or email at info@doggiewalks.co.uk</p>
                <section>
                    <img src={puppy} alt="black puppy" />
                    <div>
                        <h2>Other service we provide include: </h2>
                        <ul>
                            <li><i class="material-icons">pets</i> Pet sitting (puppy and elderly)</li>
                            <li><i class="material-icons">pets</i> Small pet visits</li>
                            <li><i class="material-icons">pets</i> Overnight pet sitting</li>
                        </ul>
                    </div>
                </section>
                <p>There's something to suit everyone but if you can't find what you're looking 
                    for don't hesitate to <span>contact us</span> and we'll find the perfect solution for 
                    you and your companion!</p>
            </article>
        </main>
    );
}
export default Home 