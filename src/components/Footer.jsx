import React from "react";

function Footer() {
  return (
      <footer>
        <section>
            <ul>
                <li>Monday - Friday: 8am – 7pm</li>
                <li>Saturday: 8am – 5pm</li>
                <li>Sunday: 12pm - 5pm</li>
                <li>01695 883 112</li>
                <li>info@doggiewalks.co.uk</li>
            </ul>
            <p>&copy; Doggie Walks</p>
            <p>Website by Zara Bostock</p>
        </section>
      </footer>
  );
}
export default Footer