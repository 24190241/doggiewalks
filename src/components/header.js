export function open() {
    var x = document.getElementById('navContainer');

    if (x.className === 'navLinks') {
        x.className += ' open';
    } else {
        x.className = 'navLinks';
    }
}