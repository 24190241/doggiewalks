import React from "react";
import charles from "./images/dog-cavalier-king-charles-spaniel-funny-pet-162193.jpeg";
import chocLab from "./images/dog-animal-pet-german-longhaired-pointer.jpg";
import blackLab from "./images/dog-black-labrador-black-dog-162149.jpeg";
import cream from "./images/pexels-photo-1487665.jpeg";
import puppy from "./images/dog-young-dog-puppy-59965.jpeg";
import corgiPuppy from "./images/pexels-photo-58997.jpeg";
import ball from "./images/pexels-photo-422220.jpeg";
import corgi from "./images/pexels-photo-976924.jpeg";
import german from "./images/pexels-photo-350428.jpeg";

function Gallery() {
    return (
        <main>
            <article className="content">
                <h1>Gallery</h1>
                <p>Every week is a busy week! Checkout how much fun our 
                    guests have within our day care centers. All photos 
                    have been taken with the consent of every owner.</p>
                <section className="gallery">
                    <img className="grid1" src={charles} alt="king charles spaniel" />
                    <img className="grid2" src={chocLab} alt="chocolate labrador" />
                    <img className="grid3" src={blackLab} alt="black labrador" />
                    <img className="grid4" src={cream} alt="cream dog" />
                    <img className="grid5" src={puppy} alt="black puppy" />
                    <img className="grid6" src={corgiPuppy} alt="corgi puppy" />
                    <img className="grid7" src={ball} alt="small dog with a ball" />
                    <img className="grid8" src={corgi} alt="corgi dog" />
                    <img className="grid9" src={german} alt="german shepard" />
                </section>
            </article>
        </main>
    );
}
export default Gallery