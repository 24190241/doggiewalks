import React from "react";
import banner from "./images/pexels-photo-264005.jpeg";
import cream from "./images/pexels-photo-1487665.jpeg";
import charles from "./images/dog-cavalier-king-charles-spaniel-funny-pet-162193.jpeg";
import lab from "./images/dog-black-labrador-black-dog-162149.jpeg";

function Service() {
    return (
        <main>
            <article className="banner">
                <img src={banner} alt="puppy pouncing in the grass" />
            </article>
            <article className="content">
               <h1>Services and Costs</h1> 
               <section className="services">
                    <div>
                        <img src={cream} alt="cream dog" />
                        <div>
                            <h2>Dog Walking</h2>
                            <p>Not all dogs enjoy group walks, therefore we provide 
                                two different services. Solo or group, discover which 
                                option is best for your dog</p>
                            <p>Duration: 1 Hour (enquire for more)</p>
                            <h3>£12/Hour</h3>
                        </div>
                    </div>
                    <div>
                        <img src={charles} alt="king charles spaniel" />
                        <div>
                            <h2>House Calls</h2>
                            <p>Need your dog looking after while you’re not home? This 
                                service allows you to have peace of mind while we ensure 
                                that your dog has the attention and care it deserves.</p>
                            <p>Duration: 1 Hour (enquire for more)</p>
                            <h3>£15/Hour</h3>
                        </div>
                    </div>
                    <div>
                        <img src={lab} alt="black labrador" />
                        <div>
                            <h2>Day Care</h2>
                            <p>Our professional day care centers provide the most up to 
                                date and clean facilities which ensures that your dog 
                                is safe, protected and healthy. This will cover all 
                                services from feeding, bathing walking and sleeping</p>
                            <p>Duration: 8 Hours per day</p>
                            <h3>£60/Day</h3>
                        </div>
                    </div>
               </section>
            </article>
            
        </main>
    );
}
export default Service;