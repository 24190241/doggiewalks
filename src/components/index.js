export { default as Navigation } from "./Navigation";
export { default as Footer } from "./Footer";
export { default as Home } from "./Home";
export { default as About } from "./About";
export { default as Team } from "./Team";
export { default as Service } from "./Service";
export { default as Gallery } from "./Gallery";
export { default as Contact } from "./Contact";