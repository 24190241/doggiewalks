import React from "react";
import { NavLink } from "react-router-dom";
import logo from "./images/logo.svg";
import { open } from "./header.js";

function Navigation() {
  return (
    <header>
      <nav className="navBar">
        <NavLink className="" to="/">
        <img src={logo} alt="Doggie Walks logo" />
        </NavLink>
        <ul className="headerNav" id="headerNav"> 
          <button onClick={ open }  id="responsive"><i class="material-icons">pets</i></button>
          <div id="navContainer" className="navLinks">
            <li className="navItem">
            <NavLink className="nav-link" to="/">
              <i class="material-icons">pets</i>Home
            </NavLink>
            </li>
            <li className="navItem">
            <NavLink className="nav-link" to="/about">
              <i class="material-icons">pets</i>About Us
            </NavLink>
            </li>
            <li className="navItem">
            <NavLink className="nav-link" to="/team">
              <i class="material-icons">pets</i>Meet the Team
            </NavLink>
            </li>
            <li className="navItem">
            <NavLink className="nav-link" to="/service">
              <i class="material-icons">pets</i>Services and Costs
            </NavLink>
            </li>
            <li className="navItem">
            <NavLink className="nav-link" to="/gallery">
              <i class="material-icons">pets</i>Gallery
            </NavLink>
            </li>
            <li className="navItem">
            <NavLink className="nav-link" to="/contact">
              <i class="material-icons">pets</i>Contact
            </NavLink>
            </li>
          </div>
        </ul>
      </nav>
    </header>
  );
}
export default Navigation