import React from "react";
import banner from "./images/pexels-photo-275717.jpeg";
import lori from "./images/team/lori-hanson.jpg";
import gary from "./images/team/gary-simpson.jpg";
import rebecca from "./images/team/rebecca-harrison.jpg";
import joanna from "./images/team/joanna-davies.jpg";

function Team() {
    return (
        <main>
            <article className="banner">
                <img src={banner} alt="dog shaking a womans hand" />
            </article>
            <article className="content">
                <h1>Meet the Team</h1>
                <p>Meet the doggie walks team! We’re fully trained and insured to 
                    work with animals and are committed to the health and benefits 
                    of your pet. We aim to keep providing the best knowledge regarding 
                    dogs as we can, and to do this we believe you cannot sit still in 
                    this occupation as we learn more about dogs daily.</p>
                <p>We ensure that our knowledge is kept up-to-date by attending regular 
                    seminars and courses presented by well-respected canine trainers, 
                    behaviorists and practitioners.</p>
                <section className="team">
                    <div>
                        <img src={lori} alt="a woman" />
                        <h2>Lori Hanson</h2>
                        <p>Founder of Doggie Walks</p>
                    </div>
                    <div>
                        <img src={gary} alt="a man" />
                        <h2>Gary Simpson</h2>
                        <p>Team Manager</p>
                    </div>
                    <div>
                        <img src={rebecca} alt="a woman" />
                        <h2>Rebbeca Harrison</h2>
                        <p>Professional Dog Walker</p>
                    </div>
                    <div>
                        <img src={joanna} alt="a woman" />
                        <h2>Joanna Davis</h2>
                        <p>Professional Dog Walker</p>
                    </div>
                </section>
            </article>
        </main>
    );
}
export default Team