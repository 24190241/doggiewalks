import React from "react";
import banner from "./images/pexels-photo-58997.jpeg";

function Contact() {
    return (
        <main>
            <article className="banner">
                <img src={banner} alt="corgi puppy" />
            </article>
            <article className="content">
                <h1>Contact Us</h1>
                <p>We have a wide variety of daycare facilities, 
                    listed below is all our locations and their 
                    address’. Feel free to pop in or fill out the form below.</p>
                <section className="addresses">
                    <ul>
                        <h2>Ormskirk</h2>
                        <li>Doggie Walks</li>
                        <li>3-5 St Helens Road,</li>
                        <li>Ormskirk,</li>
                        <li>L35 4PO</li>
                    </ul>
                    <ul>
                        <h2>Southport</h2>
                        <li>Doggie Walks</li>
                        <li>150 Lord Street,</li>
                        <li>Southport,</li>
                        <li>PR9 0NP</li>
                    </ul>
                    <ul>
                        <h2>Warrington</h2>
                        <li>Doggie Walks</li>
                        <li>47 Old Liverpool Road,</li>
                        <li>Warrington,</li>
                        <li>WA5 1AF</li>
                    </ul>
                </section>
                <form>
                    <h2>Enquire Now</h2>
                    <label htmlFor="name">Name:</label>
                    <input type="text" name="name" id="name" />
                    <label htmlFor="email">Email Address:</label>
                    <input type="email" name="email" id="email" />
                    <label htmlFor="message">Message:</label>
                    <textarea name="message" id="message" rows="10"></textarea>
                    <input type="submit" value="Send" />
                </form>
            </article>
        </main>
    );
}
export default Contact