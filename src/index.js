import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import "./nav.js";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import {
  Navigation,
  Footer,
  Home,
  About,
  Team,
  Service,
  Contact,
  Gallery,
} from "./components";


ReactDOM.render(
  <Router>
    <Navigation />
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/team" element={<Team />} />
      <Route path="/service" element={<Service />} />
      <Route path="/gallery" element={<Gallery />} />
      <Route path="/Contact" element={<Contact />} />

    </Routes>
    <Footer />
  </Router>,

  document.getElementById("root")
);

